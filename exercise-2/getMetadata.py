from urllib.request import urlopen


def getMetadata(key):
    url = 'http://169.254.169.254/latest/meta-data/' + key
    output = urlopen(url).read()
    return output.decode('utf-8')


amiId = getMetadata("ami-id")
print(amiId)

print("Getting all data...")
all = getMetadata("")
print(all)
