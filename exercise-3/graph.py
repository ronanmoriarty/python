def get(obj, key):
    keys = key.split("/")
    for key in keys:
        if key in obj:
            obj = obj[key]
        else:
            return None
    return obj
