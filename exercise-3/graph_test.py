import graph


def test_can_get_single_root_level_item_by_key():
    objectToExamine = {"foo": "bar"}
    key = "foo"
    assert graph.get(objectToExamine, key) == "bar"


def test_can_get_more_complex_root_level_item_by_key():
    objectToExamine = {"foo": {"bar": "baz"}}
    key = "foo"
    assert graph.get(objectToExamine, key) == {"bar": "baz"}


def test_can_get_nested_item_by_key():
    objectToExamine = {"foo": {"bar": "baz"}}
    key = "foo/bar"
    assert graph.get(objectToExamine, key) == "baz"


def test_returns_blank_when_key_not_found():
    objectToExamine = {"foo": {"bar": "baz"}}
    key = "does-not-exist"
    assert graph.get(objectToExamine, key) == None


def test_returns_blank_when_part_of_key_not_found():
    objectToExamine = {"foo": {"bar": "baz"}}
    key = "foo/does-not-exist"
    assert graph.get(objectToExamine, key) == None
